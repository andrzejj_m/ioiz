import matplotlib.pyplot as plt
import statistics
import numpy as np


plt.gcf().set_size_inches(15, 7)

# Dane z zadania
X = [3.1, 2.7, 3.3, 2.9, 1.1, 0.7, 1.2]
Y = [0.8, 1.2, 0.9, 1.1, 1.7, 2.1, 2.2]

# Dane z zeszłego roku
# X = [2.5, 0.5, 2.2, 1.9, 3.1, 2.3, 2, 1, 1.5, 1.1]
# Y = [2.4, 0.7, 2.9, 2.2, 3, 2.7, 1.6, 1.1, 1.6, 0.9]

# RYSUNEK 1

# plt.subplot(121)
plt.title('Oryginalne dane')

X_avg = statistics.mean(X)
Y_avg = statistics.mean(Y)

for i in range(0, len(X)):
    Y[i] = Y[i] - Y_avg
    X[i] = X[i] - X_avg

print(f'AVG X: {X_avg}\n'
      f'AVG Y: {Y_avg}')

a, b = np.polyfit(X, Y, 1)
print(f'Współczynnik kierunkowy: {a}\n'
      f'Współczynnik przesunięcia: {b}')

plt.plot(X, Y, 'go')  # oryginalne dane

plt.plot(X_avg, 0, 'b+')  # średnia X
plt.plot(0, Y_avg, 'r+')  # Średnia Y
plt.plot(X_avg, Y_avg, 'r^')  # punkt średnich X / Y

plt.plot(np.array(range(-1, 4)), a * np.array(range(-1, 4)) + b, 'b--')
plt.plot(np.array(range(-1, 4)), - a * np.array(range(-1, 4)) + b, 'r--')

plt.grid()
plt.show()
