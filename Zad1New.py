import matplotlib.pyplot as plt
import numpy as np
import scipy.sparse.linalg as linalg
from sklearn.cluster import KMeans
from sklearn.datasets import make_blobs

plt.gcf().set_size_inches(15, 7)

x = [3.1, 2.7, 3.3, 2.9, 1.1, 0.7, 1.2], [0.8, 1.2, 0.9, 1.1, 1.7, 2.1, 2.2]
X = np.array(x)


mean = X.mean(1)
norm = np.array(X) - mean[:, np.newaxis]

print("===Scentralizowany===")
print(norm)

s = norm.dot(norm.transpose())
di, wektor_charakterystyczny = linalg.eigs(s, 1)
print('===Charakterystyczny===')
print(wektor_charakterystyczny)

y_v = np.array([0, wektor_charakterystyczny[1][0]])
x_v = np.array([0, wektor_charakterystyczny[1][1]])

# na podstawie x_v i y_v liczę współczynnik kierunkowy i przesunięcia
a, b = np.polyfit(x_v, y_v, 1)
print(f'a: {a}\n'
      f'b: {b}')

rzut_y = []
for x in norm[0]:
      rzut_y.append(a * x + b)

plt.plot(X[0], X[1], 'or')
plt.plot(norm[0], norm[1], 'og')
plt.plot(x_v, y_v, 'b')
plt.plot(np.array(range(-2, 5)), a * np.array(range(-2, 5)) + b, 'b--')
plt.plot(norm[0], rzut_y, 'ob')

norm2 = []
for i in range(len(norm[0])):
      norm2.append([norm[0][i], norm[1][i]])

kmeans = KMeans(n_clusters=2, random_state=0, max_iter=300, n_init=10)
pred_y = kmeans.fit_predict(norm2)
plt.scatter(kmeans.cluster_centers_[:, 0], kmeans.cluster_centers_[:, 1], s=10000, c='orange')

plt.grid()
plt.show()
