from glob import glob
import numpy as np
from sklearn.decomposition import PCA

class PgmResolver:

    matrixes = []

    def load_pgm_file(self, file):
        with open(file, mode='r', encoding="ansi") as pgm_file:
            assert pgm_file.readline() == 'P5\n'
            (width, height) = [int(i) for i in pgm_file.readline().split()]
            depth = int(pgm_file.readline())
            assert depth <= 255

        image = np.array(np.fromfile(file, dtype=np.uint8))

        raster = []
        full_index = 0
        for y in range(height):
            row = []
            for x in range(width):
                row.append(image[full_index])
                full_index += 1
            raster.append(row)

        return raster

    def load_all_matrixes(self, faces_dir):
        for d in glob(faces_dir):
            for f in glob(d + '\\*'):
                print(f'Ładuję {f}')
                matrix = self.load_pgm_file(f)
                print(f'Załadowano {len(matrix)} x {len(matrix[0])}')
                self.matrixes.append(matrix)


pgm = PgmResolver()
pgm.load_all_matrixes('.\\faces\\*')
print(f'Wczytano {len(pgm.matrixes)} macierzy')

print('===PCA===')

pca = PCA(n_components=30)
pca.fit(pgm.matrixes[0])
print(pca.explained_variance_ratio_)